# build

ng build --prod --base-href https://angular.sumantgupta.com.np



# htaccess
RewriteCond %{SERVER_PORT} 80
RewriteCond %{HTTP_HOST} ^angular.sumantgupta\.com\.np
RewriteRule ^(.*)$ https://angular.sumantgupta\.com.np/$1 [R,L]

RewriteEngine On
    # If an existing asset or directory is requested go to it as it is
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
    RewriteRule ^ - [L]
    # If the requested resource doesnt exist, use index.html
RewriteRule ^ /index.html
