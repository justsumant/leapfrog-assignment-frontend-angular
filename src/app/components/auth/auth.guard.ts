import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable({providedIn: "root"})
export class AuthGuard implements CanActivate{
    constructor(private authService: AuthService, private router:Router){

    }

    /**
     * returns true if the user's token is already present in the browser, if used, this method is called from the route before the route leaves front end
    */
    canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean>{
        if(localStorage.getItem('token')!=undefined){
            return true;
        }else{
            this.router.navigate(['/auth']);
            return false;
        }
        
    }
}
