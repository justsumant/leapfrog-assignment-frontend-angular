import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
baseURL: string = 'http://localhost:3000';
// baseURL: string = 'https://api.sumantgupta.com.np/'


  constructor(private router: Router, private http: HttpClient) { 
    
  }

/**
 * receives user with token from backend
*/
  login(userName: String, password: String){
    let user = {userName: userName, password: password};
    return this.http.post('/auth/login', user);

  }

  signup(user:any){
    return this.http.post('/auth/signup', user);
  }


}
