import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userName: string = '';
  password: string = '';
  isProcessing: boolean = false;
  authForm: any;
  constructor(private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    this.authForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required)
    })
  }


  login() {
    this.isProcessing = true;
    this.authService.login(this.authForm.value.username, this.authForm.value.password).subscribe((data: any) => {
      if (data.token) {
        localStorage.setItem('token', data.token);
        localStorage.setItem('userId', data.loadedUserId);
        this.router.navigate(['/auth/contacts']);
      }
      this.isProcessing = false;
      this.authForm.reset();
      this.router.navigate(['/auth/contacts']);

    }, error => {
      console.log(error);

    });



  }
}
