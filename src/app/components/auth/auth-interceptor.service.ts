import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
/**
 * Interceptor class used in root provider, allows every request to be passed and filtered through this first
*/
export class authInterceptor implements HttpInterceptor{
    baseURL: string = 'http://localhost:3000';
    // baseURL: string = 'https://api.sumantgupta.com.np/';
    intercept(req: HttpRequest<any>, next: HttpHandler){
        let modifiedRequest = req.clone({
            url: this.baseURL+req.url,
            headers: req.headers.append('Authorization','Bearer ' + localStorage.getItem('token'))
        })

        
        return next.handle(modifiedRequest);
    }
}
