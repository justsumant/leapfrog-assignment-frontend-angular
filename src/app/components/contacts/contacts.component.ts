import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Contact } from 'src/app/models/contact.model';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  isProcessing: boolean = false;
  contactForm: any;
  contacts: Array<any> = [];
  favContacts: Array<any> = [];
  updateMode:boolean=false;
  isLoggedIn=false;

  constructor(private contactService: ContactService, private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('token')!=undefined){
      this.isLoggedIn=true;
    }
    // Initialise contact form reactively
    this.contactForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'contactNumber': new FormControl(null, [Validators.required, Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)]),
      'homeNumber': new FormControl(null, [Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)]),
      'officeNumber': new FormControl(null, [Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)]),
      'isFavourite': new FormControl(false, Validators.required),
      'email': new FormControl(null, [Validators.email]),
      'address': new FormControl(null, Validators.required),
      'id': new FormControl(null)
    });
    this.loadContacts();
    this.loadFavContacts();
  }

  /**
   * load contact list
  */
  loadContacts() {
    this.contactService.getContacts().subscribe((result: any) => {
      this.contacts = result;
    }, err=>{
      console.log(err);
        if (err.status == 401)
          this.router.navigate(['/']);

    });
  }

  /**
   * loads the favourite contact list
  */
  loadFavContacts() {
    this.contactService.getFavouriteContacts().subscribe((result: any) => {
      this.favContacts = result;
    }, err=>{
      console.log(err);
      if (err.status == 401)
        this.router.navigate(['/']);
    });
  }

  /**
   * adds a new contact in database
  */
  addContact() {
    this.isProcessing = true;
    const newContact: Contact = {
      name: this.contactForm.value.name,
      address: this.contactForm.value.address,
      isFavorite: this.contactForm.value.isFavourite,
      contactNumber: this.contactForm.value.contactNumber,
      email: this.contactForm.value.email,
      homeNumber: this.contactForm.value.homeNumber,
      officeNumber: this.contactForm.value.officeNumber
    }

    this.contactService.addNewContact(newContact).subscribe(result=>{
      this.isProcessing = false;
      this.loadContacts();
      this.loadFavContacts();
      this.contactForm.reset();
      this.updateMode=false;
    }, err=>{
      this.isProcessing = false;
      console.log(err);
      if (err.status == 401)
        this.router.navigate(['/']);
    })
  }

  /**
   * Deletes a contact
  */
  deleteContact(contactId: number){
    this.contactService.deleteContact(contactId).subscribe(result=>{
      this.loadContacts();
      this.loadFavContacts();
      
    }, err=>{
      console.log(err);
      if (err.status == 401)
        this.router.navigate(['/']);

      
    })
  }

  /**
   * Fills the add contact form with the existing contact that is to be updated, also changes the form mode from new entry to update
  */
  selectContactToUpdate(contact:Contact){
    this.updateMode = true;
    this.contactForm.controls['name'].setValue(contact.name);
    this.contactForm.controls['contactNumber'].setValue(contact.contactNumber);
    this.contactForm.controls['isFavourite'].setValue(contact.isFavorite);
    this.contactForm.controls['email'].setValue(contact.email);
    this.contactForm.controls['address'].setValue(contact.address);
    this.contactForm.controls['id'].setValue(contact.id);
    this.contactForm.controls['homeNumber'].setValue(contact.homeNumber);
    this.contactForm.controls['officeNumber'].setValue(contact.officeNumber);

    
  }

  /**
   * updates the selected contact
  */
  updateContact(){
    this.isProcessing = true;
    
    const newContact: Contact = {
      name: this.contactForm.value.name,
      address: this.contactForm.value.address,
      isFavorite: this.contactForm.value.isFavourite,
      contactNumber: this.contactForm.value.contactNumber,
      homeNumber: this.contactForm.value.homeNumber,
      officeNumber: this.contactForm.value.officeNumber,
      email: this.contactForm.value.email,
      id: this.contactForm.value.id
    }

    this.contactService.updateContact(newContact).subscribe(result=>{
      this.isProcessing = false;
      this.loadContacts();
      this.loadFavContacts();
      this.contactForm.reset();
      this.updateMode=false;
    }, err=>{
      this.isProcessing = false;
      console.log(err);
      if (err.status == 401)
        this.router.navigate(['/']);

      
    })
  }

  /**
   * toggles between the favourite and unfavourite state of a contact (its actually inverting the isFavourite value and updating it)
  */
  toggleFav(contact:Contact){    
    contact.isFavorite = !contact.isFavorite;
    this.contactService.updateContact(contact).subscribe(result=>{
      this.loadContacts();
      this.loadFavContacts();
    }, err=>{
      console.log(err);
      if (err.status == 401)
        this.router.navigate(['/']);

      
    })
  }


  logout(){
    localStorage.clear();
    this.router.navigate(['/']);

  }
}
