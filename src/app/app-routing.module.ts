import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './components/auth/auth.guard';
import { LoginComponent } from './components/auth/login/login.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { ContactsComponent } from './components/contacts/contacts.component';

const routes: Routes = [
  {path:'', component: LoginComponent},
  {path:'auth/contacts', canActivate:[AuthGuard], component: ContactsComponent},
  {path:'signup', component: SignupComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
