export interface Contact {
    id?: number;
    name: string;
    email: string;
    contactNumber: number;
    address: string;
    isFavorite: boolean;
    homeNumber: number;
    officeNumber: number;
}
