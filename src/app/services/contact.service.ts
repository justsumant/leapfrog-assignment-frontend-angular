import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contact } from '../models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  constructor(private http: HttpClient) { }

  getContacts(){
    return this.http.get('/getContacts');
  }

  getFavouriteContacts(){
    return this.http.get('/getFavContacts');
  }

  addNewContact(contact: Contact){
    return this.http.post('/addNewContact', contact);
  }

  deleteContact(contactId: number){
    return this.http.delete('/deleteContact' + contactId);
  }

  updateContact(contact: Contact){    
    return this.http.post('/updateContact', contact);
  }
}
